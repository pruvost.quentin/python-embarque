# Python-Embarque

Petit projet réalisé avec ADAFruit circuitplayground

## Sujet : Jeu de mémoire
Affichage de 2 LEDs pour commancer puis 3, puis 4, jusque n.
Le but est que l'utilisateur se souvienne du côté d'affichage de chacune des LEDs.
Incrémentation et décrémentation du score selon la validité de la suite saisie.

## Problème rencontré:
### Problème avec les boutons A et B
Je n'ai pas réussi à faire en sorte les bouttons ne captent que un seul clique.
En gros, mon idée de départ était de faire en sorte que l'utilisateur clique
soit sur le bouton A ou B pour indiquer le côté mais lorsque je clique sur le bouton
A ou B, ça me "simule plusieurs cliques" (voir code commenté)
Je suis passé par des inputs pour saisir les zones et non pas par les boutons.

## Avis développement embarqué
Très intéressant, dommage que l'on ai pas plus d'heures pour faire plus de choses, plus de projets.
En présentiel, je pense que ça aurait pu être encore mieux mais bon, c'était déjà très bien.