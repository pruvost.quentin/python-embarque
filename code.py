import array
import math

import time
import board
import neopixel
import digitalio
import random
from adafruit_circuitplayground import cp
 
# Couleur d'affichage des LEDs
COLOR = (50, 205, 50)
WIN = (50, 205, 50)
LOOSE = (255, 0, 0)
# Nombre de LEDs
NUM_LEDS = 10
# Liste des indices des LEDs
LISTE_INDICE_LED = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# Temps d'attente entre l'affichge des différentes LEDs
WAIT = 1
# Temps d'affichge du résultat
WAIT2 = 3
# Constantes
ZONE_A = "ZONE A"
ZONE_B = "ZONE B"
n = 2

# Extinction de toutes les LED
pixels = cp.pixels
pixels.fill(0)
pixels.show()

""" buttonA = digitalio.DigitalInOut(board.BUTTON_A)
buttonA.switch_to_input(pull=digitalio.Pull.DOWN)

buttonB = digitalio.DigitalInOut(board.BUTTON_B)
buttonB.switch_to_input(pull=digitalio.Pull.DOWN) """

# Fonction de comparaison de 2 lists
def compareLists(l1, l2):
    same = True

    for i in range(len(l1) - 1):
        if l1[i] != l2[i]:
            same = False
    
    return same

# Affichage du résultat (LEDs rouge ou verte) en fonction d'un booléen
def affichageResultat(rgb):

    pixels.fill(0)
    for i in range(NUM_LEDS):
        pixels[i] = rgb
    pixels.show()
    time.sleep(WAIT2)
    pixels.fill(0)

# Fonction qui permet de mélanger la liste d'indices des LEDs
def randomiseList():
    for i in range(len(LISTE_INDICE_LED)-1, 0, -1): 
        
        # Pick a random index from 0 to i  
        j = random.randint(0, i + 1)  
        
        # Swap arr[i] with the element at random index  
        LISTE_INDICE_LED[i], LISTE_INDICE_LED[j] = LISTE_INDICE_LED[j], LISTE_INDICE_LED[i]

# Fonction qui permet d'afficher les LEDs une à une et
# de mettre les zones correspondantes dans un tableau qui est retounré
def allumerLED(n):

    if n == 2:
        randomiseList()

    tabResultLED = []
    pixels.fill(0)
    for i in range(n):
        indiceLED = LISTE_INDICE_LED[i] - 1
        pixels[indiceLED] = COLOR
        if indiceLED < 5:
            tabResultLED.append(ZONE_A)
        else:
            tabResultLED.append(ZONE_B)
        time.sleep(WAIT)
        pixels.show()

    time.sleep(WAIT)
    pixels.fill(0)
    pixels.show()
    
    return tabResultLED

""" def clickBoutton():
    nbClick = 0
    tabResult = []

    while nbClick < 10:
        if buttonA.value:
            tabResult.append(ZONE_A)
            nbClick = nbClick + 1
        elif buttonB.value:
            tabResult.append(ZONE_B)
            nbClick = nbClick + 1
        
    return tabResult """

while True:
    
    # Initialisation du score
    score = 0

    # Boucle du jeu
    while n < 11:     
        x = allumerLED(n)
        # print (x)
        a = []
        # Affichage de n input (n = nombre de LEDs qui se sont allumées)
        for i in range(n):
            b = input('Entrez la zone (ZONE A ou ZONE B) : ')
            a.append(b.upper())
        
        # Comparaison des 2 listes
        s = compareLists(x, a)

        # Condition selon le résultat de la comparaison des 2 listes
        if s == True:
            print("Correct!")
            cp.play_tone(262, 1)
            affichageResultat(WIN)
            
            # Incrémentation du nombre de LEDs à afficher
            n = n+1
            # Incrémentation du score
            score = score + 1
        else:
            print('FAUX! try again')
            cp.play_tone(175, 1)
            affichageResultat(LOOSE)
            
            score = score - 1

    # Affichage du score final
    print("Score final", score)
    break
        
        